/*
*  Name:  index.js
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2021 Codepoint Technologies, Inc.
*  All Rights Reserved
*/

const Subjects      = require('./subjects');
//const Events        = require('./events');
const ApiPlugin     = require('./apiPlugin');
const CodecPlugin   = require('./codecPlugin');
const CommRedirectPlugin   = require('./commRedirectPlugin');
const ControllerPlugin   = require('./controllerPlugin');
const ImageCachePlugin   = require('./imageCachePlugin');

module.exports = {
    Subjects: Subjects,
    ApiPlugin :ApiPlugin,
    CodecPlugin : CodecPlugin,
    CommRedirectPlugin :   CommRedirectPlugin,
    ControllerPlugin : ControllerPlugin,
    ImageCachePlugin : ImageCachePlugin
};
